const util = require('util');
const { MongoClient } = require('mongodb');

const URI = 'mongodb://localhost?retryWrites=true&w=majority';

const client = new MongoClient(URI);

const objects = [
  {
    id: 'foo',
    name: 'Foo',
    avatar: 'https://example.com/foo.jpg'
  },
  {
    id: 'bar',
    name: 'Bar',
    avatar: 'https://example.com/bar.jpg'
  },
  {
    id: 'zoo',
    name: 'Zoo',
    avatar: 'https://example.com/zoo.jpg'
  },
  {
    id: 'mar',
    name: 'Mar',
    avatar: 'https://example.com/mar.jpg'
  },
  {
    id: 'zee',
    name: 'Zee',
    avatar: 'https://example.com/zee.jpg'
  },
];

async function run() {
  try {
    await client.connect();
    const database = client.db('benchmark');
    const test = database.collection('test');
    await test.createIndex({'obj.id': 1});
    await test.createIndex({'obj.name': 1});
    await test.createIndex({'obj.avatar': 1});
    await test.createIndex({'obj.avatar': 1, 'obj.name': 1});

    console.log("Cleaning collection.");
    await test.deleteMany({});

    console.log("Preparing data to insert.");
    const documents = [];
    for (let i = 0; i < 1000000; i++) {
      documents.push({
        obj: objects[i % objects.length],
      });
    }

    console.log("Inserting.");
    await test.insertMany(documents);

    await database.setProfilingLevel('all');
    const profile = database.collection('system.profile');

    await test.updateMany({
      id: 'foo',
      $or: [
        {
          'obj.name': {$ne: 'Foo'},
        },
        {
          'obj.avatar': {$ne: 'https://example.com/foo.jpg'},
        },
      ],
    }, {
      $set: {
        'obj.name': 'Foo',
        'obj.avatar': 'https://example.com/foo.jpg',
      },
    });
    console.log(util.inspect((await profile.findOne({}, {sort: [['$natural', -1]]})), {depth: null}));

    await test.updateMany({
      id: 'foo',
    }, {
      $set: {
        'obj.name': 'Foo',
        'obj.avatar': 'https://example.com/foo.jpg',
      },
    });
    console.log(util.inspect((await profile.findOne({}, {sort: [['$natural', -1]]})), {depth: null}));
  } finally {
    await client.close();
  }
}

run().catch(console.dir);
