Start MongoDB:

```sh
$ docker run --rm -d --name mongo -p 27017:27017 mongo:4.4.1
```

Running the following `updateMany` query:

```js
{
  id: 'foo',
  $or: [
    {
      'obj.name': {$ne: 'Foo'},
    },
    {
      'obj.avatar': {$ne: 'https://example.com/foo.jpg'},
    }
  ],
},
{
  $set: {
    'obj.name': 'Foo',
    'obj.avatar': 'https://example.com/foo.jpg',
  },
}
```

Profiler:

```js
{
  op: 'update',
  ns: 'benchmark.test',
  command: {
    q: {
      id: 'foo',
      '$or': [
        { 'obj.name': { '$ne': 'Foo' } },
        { 'obj.avatar': { '$ne': 'https://example.com/foo.jpg' } }
      ]
    },
    u: {
      '$set': {
        'obj.name': 'Foo',
        'obj.avatar': 'https://example.com/foo.jpg'
      }
    },
    multi: true,
    upsert: false
  },
  keysExamined: 1600002,
  docsExamined: 800000,
  nMatched: 0,
  nModified: 0,
  numYield: 1601,
  queryHash: '3338F0A3',
  planCacheKey: 'A7A0C9C0',
  locks: {
    ParallelBatchWriterMode: { acquireCount: { r: 1602 } },
    ReplicationStateTransition: { acquireCount: { w: 1603 } },
    Global: { acquireCount: { r: 1, w: 1602 } },
    Database: { acquireCount: { w: 1602 } },
    Collection: { acquireCount: { w: 1602 } },
    Mutex: { acquireCount: { r: 1 } }
  },
  flowControl: { acquireCount: 1602, timeAcquiringMicros: 7371 },
  storage: {},
  millis: 2327,
  planSummary: 'IXSCAN { obj.name: 1 }, IXSCAN { obj.avatar: 1 }',
  execStats: {
    stage: 'UPDATE',
    nReturned: 0,
    executionTimeMillisEstimate: 0,
    works: 1,
    advanced: 0,
    needTime: 0,
    needYield: 0,
    saveState: 1601,
    restoreState: 1601,
    isEOF: 1,
    nMatched: 0,
    nWouldModify: 0,
    wouldInsert: false,
    inputStage: {
      stage: 'CACHED_PLAN',
      nReturned: 0,
      executionTimeMillisEstimate: 2297,
      works: 0,
      advanced: 0,
      needTime: 0,
      needYield: 0,
      saveState: 1601,
      restoreState: 1601,
      isEOF: 1,
      inputStage: {
        stage: 'FETCH',
        filter: { id: { '$eq': 'foo' } },
        nReturned: 0,
        executionTimeMillisEstimate: 348,
        works: 1600004,
        advanced: 0,
        needTime: 1600003,
        needYield: 0,
        saveState: 1601,
        restoreState: 1601,
        isEOF: 1,
        docsExamined: 800000,
        alreadyHasObj: 0,
        inputStage: {
          stage: 'OR',
          nReturned: 800000,
          executionTimeMillisEstimate: 223,
          works: 1600004,
          advanced: 800000,
          needTime: 800003,
          needYield: 0,
          saveState: 1601,
          restoreState: 1601,
          isEOF: 1,
          dupsTested: 1600000,
          dupsDropped: 800000,
          inputStages: [
            {
              stage: 'IXSCAN',
              nReturned: 800000,
              executionTimeMillisEstimate: 71,
              works: 800002,
              advanced: 800000,
              needTime: 1,
              needYield: 0,
              saveState: 1601,
              restoreState: 1601,
              isEOF: 1,
              keyPattern: { 'obj.name': 1 },
              indexName: 'obj.name_1',
              isMultiKey: false,
              multiKeyPaths: { 'obj.name': [] },
              isUnique: false,
              isSparse: false,
              isPartial: false,
              indexVersion: 2,
              direction: 'forward',
              indexBounds: { 'obj.name': [ '[MinKey, "Foo")', '("Foo", MaxKey]' ] },
              keysExamined: 800001,
              seeks: 2,
              dupsTested: 0,
              dupsDropped: 0
            },
            {
              stage: 'IXSCAN',
              nReturned: 800000,
              executionTimeMillisEstimate: 64,
              works: 800002,
              advanced: 800000,
              needTime: 1,
              needYield: 0,
              saveState: 1601,
              restoreState: 1601,
              isEOF: 1,
              keyPattern: { 'obj.avatar': 1 },
              indexName: 'obj.avatar_1',
              isMultiKey: false,
              multiKeyPaths: { 'obj.avatar': [] },
              isUnique: false,
              isSparse: false,
              isPartial: false,
              indexVersion: 2,
              direction: 'forward',
              indexBounds: {
                'obj.avatar': [
                  '[MinKey, "https://example.com/foo.jpg")',
                  '("https://example.com/foo.jpg", MaxKey]'
                ]
              },
              keysExamined: 800001,
              seeks: 2,
              dupsTested: 0,
              dupsDropped: 0
            }
          ]
        }
      }
    }
  },
  ts: 2020-11-14T00:19:00.652Z,
  client: '172.17.0.1',
  allUsers: [],
  user: ''
}
```

Running the following `updateMany` query:

```js
{
  id: 'foo'
},
{
  $set: {
    'obj.name': 'Foo',
    'obj.avatar': 'https://example.com/foo.jpg',
  },
}
```

Profiler:

```js
{
  op: 'update',
  ns: 'benchmark.test',
  command: {
    q: { id: 'foo' },
    u: {
      '$set': {
        'obj.name': 'Foo',
        'obj.avatar': 'https://example.com/foo.jpg'
      }
    },
    multi: true,
    upsert: false
  },
  keysExamined: 0,
  docsExamined: 1000000,
  nMatched: 0,
  nModified: 0,
  numYield: 1000,
  queryHash: '6DAB46EC',
  planCacheKey: '6DAB46EC',
  locks: {
    ParallelBatchWriterMode: { acquireCount: { r: 1001 } },
    ReplicationStateTransition: { acquireCount: { w: 1002 } },
    Global: { acquireCount: { r: 1, w: 1001 } },
    Database: { acquireCount: { w: 1001 } },
    Collection: { acquireCount: { w: 1001 } },
    Mutex: { acquireCount: { r: 1 } }
  },
  flowControl: { acquireCount: 1001, timeAcquiringMicros: 4267 },
  storage: {},
  millis: 470,
  planSummary: 'COLLSCAN',
  execStats: {
    stage: 'UPDATE',
    nReturned: 0,
    executionTimeMillisEstimate: 35,
    works: 1000002,
    advanced: 0,
    needTime: 1000001,
    needYield: 0,
    saveState: 1000,
    restoreState: 1000,
    isEOF: 1,
    nMatched: 0,
    nWouldModify: 0,
    wouldInsert: false,
    inputStage: {
      stage: 'COLLSCAN',
      filter: { id: { '$eq': 'foo' } },
      nReturned: 0,
      executionTimeMillisEstimate: 34,
      works: 1000002,
      advanced: 0,
      needTime: 1000001,
      needYield: 0,
      saveState: 1000,
      restoreState: 1000,
      isEOF: 1,
      direction: 'forward',
      docsExamined: 1000000
    }
  },
  ts: 2020-11-14T00:19:01.134Z,
  client: '172.17.0.1',
  allUsers: [],
  user: ''
}
```

Running an update over many documents updating all fields associated with an `id` is much
faster (470 ms) if you do not check if values have changed (2327 ms). It follows that this
attempt at optimization to minimize writes makes things worse. The reason is that `$ne`
is not [very selective](https://docs.mongodb.com/manual/core/query-optimization/#read-operations-query-selectivity)
and requires a scan over documents to find documents which match.

It is interesting to note that execution estimate is close in the later case
(2297 ms estimate vs. 2327 ms real) while for the former case it is off
(35 ms estimate vs. 470 ms real).

